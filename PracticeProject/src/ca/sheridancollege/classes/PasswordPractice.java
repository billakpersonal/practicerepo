package ca.sheridancollege.classes;

public class PasswordPractice {

	public static boolean checkLength(String password) {
		
		return password.length()>=8?true:false;
	}

	public static boolean checkDigits(String password) {
		
		return password.matches("(.)*(\\d)(.)*(\\d)(.)*");
	}

	public static boolean validatePassword(String password) {
		
		return checkLength(password) && checkDigits(password);
	}

}
