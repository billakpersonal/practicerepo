package ca.sheridancollege.tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import ca.sheridancollege.classes.Password;
/**
 * Name : Krishan Billa, Sagar Gandhi, Chandan Thukral
 * Student Id : 991417224, 991418134, 991393833
 * Assignment : ICE 03
 * 
 * Password Test Class with all possible test cases
 */
public class Test {

	
	
	@org.junit.Test
	public void testLengthLess() {
		boolean result = Password.checkLength("hello");
		assertTrue("Result should be false",result==false);
	}
	@org.junit.Test
	public void testDigitLess() {
		boolean result = Password.checkDigit("hello");
		assertTrue("Result should be false",result==false);
	}
	
	@org.junit.Test
	public void testLengthExact() {
		boolean result = Password.checkLength("password");
		assertTrue("Result should be true ", result==true);
	}
	@org.junit.Test
	public void testLengthBoundary() {
		boolean result = Password.checkLength("qwertyu");
		assertTrue("Result should be true ", result==false);
	}
	@org.junit.Test
	public void testDigitNone() {
		boolean result = Password.checkDigit("password");
		assertTrue("Result should be true ", result==false);
	}
	
	@org.junit.Test
	public void testDigitExact() {
		boolean result= Password.checkDigit("12");
		assertTrue("Result should be true",result==true);
		
	}
	@org.junit.Test
	public void testDigitBoundary() {
		boolean result= Password.checkDigit("1");
		assertTrue("Result should be true",result==false);
		
	}
	@org.junit.Test
	public void testDigitMore(){
		boolean result= Password.checkDigit("123");
		assertTrue("Result should be true",result==true);
		
	}
	@org.junit.Test
	public void testEmptyPassword() {
		boolean result = Password.validatePassword("");
		assertTrue("Result should be false ", result==false);
	}
	@org.junit.Test
	public void testWeakPassword() {
		boolean result = Password.validatePassword("qweryui1");
		assertTrue("Result should be false ", result==false);
	}
	@org.junit.Test
	public void testStrongPassword() {
		boolean result = Password.validatePassword("qwertyui12");
		assertTrue("Result should be false ", result==true);
	}
}
