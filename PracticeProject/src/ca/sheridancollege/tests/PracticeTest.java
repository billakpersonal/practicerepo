package ca.sheridancollege.tests;

import static org.junit.Assert.*;

import org.junit.Test;

import ca.sheridancollege.classes.PasswordPractice;

public class PracticeTest {

	@Test
	public void testCheckLength() {
		boolean result = PasswordPractice.checkLength("12345");
		assertTrue("Result should be false",result==false);
	}

	@Test
	public void testTwoDigits() {
		boolean result=PasswordPractice.checkDigits("11");
		assertTrue("Result is "+result,result==true);
	}
	
	@Test
	public void testValidatePassword() {
		boolean result = PasswordPractice.validatePassword("billak1aa");
		assertTrue("Result is false",result==false);
	}
}
