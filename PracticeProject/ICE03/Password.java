package ca.sheridancollege.classes;

/**
 * Name : Krishan Billa, Sagar Gandhi, Chandan Thukral
 * Student Id : 991417224, 991418134, 991393833
 * Assignment : ICE 03
 * 
 * Password Class with to check length, no. of digits and validate password
 */

public class Password {

	//Phase 1  -- Methods Generated All tests fail
//	public static boolean checkLength(String password) {
//		return true;
//	}
//
//	public static boolean checkDigit(String password) {
//		return true;
//	}
//	public static boolean validatePassword(String password) {
//		return true;	
//	}
	
//Phase 2  - All methods returning true, to make the tests pass
//	public static boolean checkLength(String password) {
//		return false;
//	}
//
//	public static boolean checkDigit(String password) {
//		return false;
//	}
//	public static boolean validatePassword(String password) {
//		return false;	
//	}
//
	
	
	
	
	//Phase 3 -- Implementing Logic to make tests pass
	public static boolean checkLength(String password) {
		
		if(password.length()>=8) {
			return true;
		}
		return false;
	}

	public static boolean checkDigit(String password) {
	
		int count=0;
		for(int a =0;a<password.length();a++)
		{
		
		char c = password.charAt(a);
		if(Character.isDigit(c))
		{
			count++;
		}
		
		}
		if(count>=2) {
			return true;
		}
		return false;
	}
	
	
	public static boolean validatePassword(String password) {
		if(checkLength(password)&&checkDigit(password)) return true;
		return false;
		
	}
}
